#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 17:15:55 2020

@author: stem
"""
#%%
import numpy as np
import cartopy.crs as ccrs

#%%
def rotpole2wgs(rlon, rlat, pollon, pollat, inverse=False):
    """
    Transform rotated pole to WGS84.
    """
    c_in = ccrs.RotatedPole(pollon, pollat)
    c_out = ccrs.PlateCarree()

    if inverse:
        c_in, c_out = c_out, c_in

    if np.ndim(rlon) == 0:
        res = c_out.transform_point(rlon, rlat, c_in)
        return res[0], res[1]
    elif np.ndim(rlon) in [1,2]:
        res = c_out.transform_points(c_in, rlon, rlat)
        return res[...,0], res[...,1]
    else:
        shape = rlon.shape
        res = c_out.transform_points(c_in, rlon.flatten(), rlat.flatten())
        return res[:,0].reshape(shape), res[:,1].reshape(shape)
    
    

#%%
def interpolate_wind2masspoints(u,v):
    """\
    Unstagger wind components from the cell borders to the cell centers.

    u: horizontal wind component in x-direction
    v: horizontal wind component in y-direction
    """

    ur = 0.5 * (u[...,:,:-1] + u[...,:,1:])
    vr = 0.5 * (v[...,:-1,:] + v[...,1:,:])
    
    ur = np.insert(ur, [0], 0., axis=3)
    vr = np.insert(vr, [0], 0., axis=2)

    return ur, vr



#%%
def rot2geographical_wind_components(u,v,rlon,rlat,pollon,pollat):
    """\
    Compute wind components relative to geographical coordinates.

    u: horizontal wind component in x-direction relative to rotated grid
    v: horizontal wind component in y-direction relative to rotated grid
    rlon: rotated longitudes
    rlat: rotated latitudes
    pollon: rotated pole longitude
    pollat: rotated pole latitude
    """

    rlon, rlat = np.meshgrid(rlon, rlat)
    lon, lat = rotpole2wgs(rlon, rlat, pollon, pollat, inverse=False)

    pollon = np.deg2rad(pollon)
    pollat = np.deg2rad(pollat)
    lon    = np.deg2rad(lon)
    lat    = np.deg2rad(lat)
    
    delta = np.arctan( (np.cos(pollat)*np.sin(pollon-lon)) / ( (np.cos(lat)*np.sin(pollat)) -
                       (np.sin(lat)*np.cos(pollat)*np.cos(pollon-lon)) ) )

    if np.ndim(u)==4:
        delta = np.tile(delta,(1,u.shape[1],1,1))
    elif np.ndim(u)==3:
        delta = np.tile(delta,(u.shape[0],1,1))
    elif np.ndim(u)==2:
        delta = np.tile(delta,(1,1))

    ug = u * np.cos(delta) + v * np.sin(delta)
    vg = -u * np.sin(delta) + v * np.cos(delta)

    return ug, vg