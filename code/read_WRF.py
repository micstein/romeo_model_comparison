#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 21:52:18 2020
@author: stem
"""
#%%
from datetime import datetime, timedelta
import numpy as np
import os
from netCDF4 import Dataset
from cdo import Cdo
from namelist import *

#%%
cdo = Cdo()

#%%
def readLSCE(map_height,date_obs,CH4_vars_map):
    
    #Select timesteps "affected" by flight-period:
    ii = 0
    enddate  = date_obs[-1]
    loopdate = date_obs[0]
    datelist = []
    while (loopdate <= enddate):
        loopdate = (date_obs[0]+timedelta(minutes=30)).replace(
            minute=0,second=0
        ) + timedelta(hours=ii)
        if loopdate>(enddate+timedelta(minutes=30)):
            continue
        datelist = np.append(datelist,loopdate)
        ii+=1
    
    #Initiate dictionary and list of variable-names:
    wrf_names = []
    for cname in CH4_vars_map: #read from lok-up table:
        wrf_names = np.append(wrf_names, CH4names[cname]["LSCE"])
    
    print("---------------------------------------------------------")
    print("Remapping WRF-output...")
    
    #Go through all timesteps, read variables and save in "normal" dimensions:
    for idate,filedate in enumerate(datelist):
        filename = "WRF_%s00.nc" % (filedate.strftime("%Y%m%d%H"))
        print("     ...%s" % (filedate.strftime("%Y%m%d%H")))
        file = os.path.join(wrfoutput,filename)
        #Read the input-file of this timestep, select the variables,...
        #...calculate height and make diemensions lat/lon:
        src = Dataset(file)
        dst = Dataset("%s/wrfoutputfile.nc" % (wrfoutput), "w")
        wrf_lats = src["XLAT"][0,...]
        wrf_lons = src["XLONG"][0,...]
        #Create dimensions for nc-file:
        olat = dst.createDimension("lat", wrf_lats.shape[0])
        olon = dst.createDimension("lon", wrf_lons.shape[1])
        olev = dst.createDimension("level", len(src.dimensions["bottom_top"]))
        olon = dst.createVariable("lon", np.float32, ("lat", "lon"))
        olat = dst.createVariable("lat", np.float32, ("lat", "lon"))
        olev = dst.createVariable("level", np.float32, ("level",))
        olon.axis = "X"
        olon.units = "degrees"
        olon.standard_name = "longitude"
        olat.axis = "Y"
        olat.units = "degrees"
        olat.standard_name = "latitude"
        olev.axis = "Z"
        olev.units = "level"
        olev.standard_name = "level"
        olon[:] = wrf_lons
        olat[:] = wrf_lats
        olev[:] = np.arange(len(src.dimensions["bottom_top"]))
        #Calculate height above ground:
        AGL = (np.array(src["PH"][0,...])+np.array(src["PHB"][0,...])) / 9.81
        AGL = 0.5*(AGL[:-1,...] + AGL[1:,...])
        for ih in np.arange(AGL.shape[0]):
            AGL[ih,...] -= np.array(src.variables["HGT"][0,...])
        #Write height to nc-file:
        oh = dst.createVariable("AGL", np.float32, ("level", "lat", "lon"))
        oh.units = "m"    
        oh[:] = AGL
        #Go through the tracers (and wind and T):
        for name, variable in src.variables.items():
            if name not in ['U','V','T'] and 'tracer' not in name:
                continue
            #Bring wind from staggered grid to non-staggered grid:
            if name == "U":
                field = 0.5*(src[name][:,:,:,:-1] + src[name][:,:,:,1:])
            elif name == "V":
                field = 0.5*(src[name][:,:,:-1,:] + src[name][:,:,1:,:])
            else:
                field = src[name][...,:,:,:]
            #Write to nc-file as fct of lon/lat/levels:
            units = src[name].units
            oh = dst.createVariable(name, np.float32, ("level", "lat", "lon"))
            oh.units = units       
            oh[:] = field
        src.close()
        dst.close()

        #Interpolate with cdo to target-height:
        cdo.intlevel3d(
            "../data/grids/hhl.nc %s/wrfoutputfile.nc \
            ../data/grids/nc_height.nc" % (wrfoutput),
            output = "../data/grids/intheight.nc"
        )
        
        
        #Remap to specific grid with cdo:
        cdo.remapcon("../data/grids/outgrid.txt -selname,"
            + ",".join(map(str,np.append(wrf_names, ["U","V","T"])))
            + " -setgrid,../data/grids/wrf_in_grid.txt",
            input = "../data/grids/intheight.nc",
            output = "regridded%s00.nc" % (filedate.strftime("%Y%m%d%H"))
        )
        
        #Remove these intermediate files:      
        os.remove("../data/grids/intheight.nc")
        os.remove("%s/wrfoutputfile.nc" % (wrfoutput))
    
    print("---------------------------------------------------------")
    print("Reading WRF-output...")
    
    #Go through all timesteps and open/read file:
    fields = []
    for idate,filedate in enumerate(datelist):
        filename = "regridded%s00.nc" % (filedate.strftime("%Y%m%d%H"))
        print("     ...%s" % (filedate.strftime("%Y%m%d%H")))
        file = os.path.join(filename)
        data = Dataset(file)
        ###Read data
        CH4_field = np.zeros( (data["tracer_3"][0,...]).shape )
        lat = data["lat"]
        lon = data["lon"]
        U = data["U"][0,...]
        V = data["V"][0,...]
        for xy_tracer in wrf_names:
            CH4_field += 0.001*np.array( #ppb --> ppm
                data.variables["%s" % (xy_tracer)][0,...]
            )
        
        #Remove the regridded file:
        os.remove(filename)
        
        #Replace filled values by nan:
        CH4_field[CH4_field<-900] = np.nan
        
        #Save to dictionary:
        fields = np.append(fields,
                    {
                        "CH4": CH4_field + 1.925,
                        "date": filedate,
                        "lat": lat,
                        "lon": lon,
                        "U": U,
                        "V": V 
                    }
                )
    
    return fields


#%%
def readEXTRACTED_LSCE(
    model,
    flight_date,
    CH4_staggered,
    CH4_vars_map,
    mission,
    flight_start,
    flight_end,
    flight_nr):
    
    print('---------------------------------------------------------')
    print('Reading extraced LSCE model data...')
    
    dicts_curtain = {}  
    dicts_extracted = {}
    
    return dicts_curtain, dicts_extracted