#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 21:50:56 2020
@author: stem
"""
#%%
import numpy as np
from netCDF4 import Dataset
import os
from datetime import timedelta
from cdo import Cdo
from namelist import *
from auxiliary_functions import *

#%%
cdo = Cdo()

#%%
def readEMPA(map_height,date_obs,CH4_vars_map):
    
    #Select the timesteps, which fall into the flight-period:
    ii = 0
    enddate = date_obs[-1]
    loopdate = date_obs[0]
    datelist = []
    while (loopdate <= enddate):
        loopdate = (date_obs[0] + timedelta(minutes=30)).replace(
            minute=0,second=0
        ) + timedelta(hours=ii)
        #If the date is more than 30 minutes after end of flight:
        if loopdate > (enddate+timedelta(minutes=30)):
            continue
        datelist = np.append(datelist,loopdate)
        ii += 1

    print("---------------------------------------------------------")
    print("Remapping COMSO-output...")
    
    #Go through all timesteps and remap file with cdo:
    for idate,filedate in enumerate(datelist):
        filename = "lffd%s.nc" % (filedate.strftime("%Y%m%d%H"))
        print("     ...%s" % (filedate.strftime("%Y%m%d%H")))
        file = os.path.join(cosmooutput,filename)
        
        #Select variables:
        cdo.selname(",".join(
            map(str,np.append(CH4_vars_map,["U", "V", "T", "QV" ]))
            ),
            input = file,
            output = "selection%i.nc" % (idate)
        )
        
        with Dataset("selection%i.nc" % (idate)) as src, Dataset("geowind%i.nc" % (idate), "w") as dst:
            # copy attributes
            for name in src.ncattrs():
                dst.setncattr(name, src.getncattr(name))
            # copy dimensions
            for name, dimension in src.dimensions.items():
                dst.createDimension(
                    name, (len(dimension) if not dimension.isunlimited else None))
            # copy all file data except for the excluded
            for name, variable in src.variables.items():
                if name not in ["U", "V"]:
                    x = dst.createVariable(name, variable.datatype, variable.dimensions)
                    dst.variables[name][:] = src.variables[name][:]
                else:
                    U_rot = src.variables["U"][:]
                    V_rot = src.variables["V"][:]
                    U_unstag, V_unstag = interpolate_wind2masspoints(U_rot,V_rot)
                    rlons = np.linspace(startlon, endlon, 450)
                    rlats = np.linspace(startlat, endlat, 300)
                    U, V = rot2geographical_wind_components(U_unstag,V_unstag,rlons,rlats,pollon,pollat)
                    x = dst.createVariable(name, variable.datatype, variable.dimensions)
                    dst.variables[name][:] = src.variables[name][:]
            
        #Interpolate to specific height:
        cdo.intlevel3d(
            "../data/grids/hhl.nc geowind%i.nc ../data/grids/nc_height.nc" % (idate),
            output = "intheight%i.nc"  % (idate)
            )

        #Remap to specific grid:
        cdo.remapcon(
            "../data/grids/outgrid.txt"
            + " -setgrid,../data/grids/cosmo_in_grid.txt",
            input = "intheight%i.nc" % (idate),
            output = "regridded%s.nc" % (filedate.strftime("%Y%m%d%H"))
            )

        #Remove the intermediate file:
        os.remove("intheight%i.nc"  % (idate))
        os.remove("geowind%i.nc"  % (idate))
        os.remove("selection%i.nc"  % (idate))

    #Read regridded files and save to dictionary:
    print('---------------------------------------------------------')
    print('Reading COMSO-output...')
    #Go through all timesteps and open/read file:
    fields = []
    for idate,filedate in enumerate(datelist):
        filename = "regridded%s.nc" % (filedate.strftime("%Y%m%d%H"))
        print("     ...%s" % (filedate.strftime("%Y%m%d%H")))
        file = os.path.join(filename)
        data = Dataset(file)
        
        #Read data
        CH4_field = np.zeros( (data.variables['CH4_BG'][0,0,...]).shape )
        lat = (data.variables["lat"][:])
        lon = (data.variables["lon"][:])
        U = (data.variables["U"][0,0,...])
        V = (data.variables["V"][0,0,...])
        
        for xy_tracer in CH4_vars_map:
            #correct for the wrong CH4_LAKES-values in our output:
            if xy_tracer == "CH4_LAKES":
                CH4_field += (0.001/4944753.17)*np.array(data.variables["%s" % (xy_tracer)][0,0,...])
            else:
                CH4_field += 0.001*np.array(data.variables["%s" % (xy_tracer)][0,0,...]) ###ppb --> ppm   
        
        #Remove the regridded file:
        os.remove(filename)
        
        #Save to dictionary:
        fields = np.append(
            fields,
            {
                "CH4": CH4_field,
                "date": filedate,
                "lat": lat,
                "lon": lon,
                "U": U,
                "V": V
            } )
    
    return fields

#%%
def readEXTRACTED_EMPA(model,
                       flight_date,
                       CH4_staggered,
                       CH4_vars_map,
                       mission,
                       flight_start,
                       flight_end,
                       flight_nr):

    print('---------------------------------------------------------')
    print('Reading extraced model data...')
    
    #There are 2 formats of variable names for this output:
    infile1 = "_%s_%s_romeo-reanalysis-002_%s" % (
        mission,
        flight_date.strftime('%Y-%m-%d'),
        flight_date.strftime('%Y%m%d')
        )
    infile2 = "_%s_%s_romeo-reanalysis-002_%s" % (
        mission,
        flight_date.strftime('%d%m%Y'),
        flight_date.strftime('%Y%m%d')
        )
    if flight_date.strftime("%d%m%Y") == "03102019" and mission == 'INCAS':
        infile2 = "_%s_%i-%s_romeo-reanalysis-002_%s" % (
            mission,
            flight_nr,
            flight_date.strftime("%d%m%Y"),
            flight_date.strftime("%Y%m%d")
            )
    
    #Select the correct file name:
    for flightfile in os.listdir(cosmoextract):
        if infile1 in flightfile or infile2 in flightfile:
            infile = flightfile
    
    #Open the according curtain-file:
    fname = os.path.join(cosmoextract,infile)
    fh = Dataset(fname, mode="r")
    
    #Initiate dictionaries and list of variable names:
    dicts_curtain = {}   
    dicts_extracted = {}   
    varlist = model_vars_cosmo

    #Create date from epoch-time:
    ftime = np.array(fh.variables["time"][:])
    date_ext = []
    for isec in ftime:
        date_ext = np.append(date_ext, date_ref + timedelta(seconds=int(isec)))

    #Set start and end of flight:
    if not flight_start:
        flight_start = date_ext[0]
    if not flight_end:
        flight_end = date_ext[-1]   

    #Add the tracers from the staggered plot to the variable-list:
    seen = set()
    CH4_list = list(
        [
            item
            for innerlist in CH4_staggered
            for item in innerlist
            if item not in seen and not seen.add(item)
        ] )
    for x in CH4_list:
        varlist.append(x + "_f")
        varlist.append(x + "_c")

    #Add the tracers from the map and timeseries: to the variable-list
    for x in CH4_vars_map:
        varlist.append(x + "_f")
        varlist.append(x + "_c")
    
    #Remove double entries in the variable-list:
    seen = set()
    varlist = [
        item for item in varlist if item not in seen and not seen.add(item)
        ]
    
    #Write data of the variables to dictionary:
    for var in varlist:
        if var.endswith("_f"):
            dicts_extracted.__setitem__(
                var,np.array(fh.variables[var][(date_ext>flight_start)&(date_ext<flight_end)])
                )
        if var.endswith("_c"):
            dicts_curtain.__setitem__(
                var,np.array(fh.variables[var][(date_ext>flight_start)&(date_ext<flight_end)])
                )
    #Make it a numpy-array and add time:
    dicts_curtain = np.array([dicts_curtain])
    dicts_extracted.__setitem__(
        "time",
            np.array(fh.variables['time'][(date_ext>flight_start)&(date_ext<flight_end)])
        )
    dicts_extracted = np.array([dicts_extracted])
    
    #Replace fill values by nan:
    for var in dicts_extracted[0]:
        for ix,x in enumerate(dicts_extracted[0][var]):
            if x<-900:
                dicts_extracted[0][var][ix]=np.nan
    
    return dicts_curtain, dicts_extracted
