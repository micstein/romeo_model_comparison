#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 21:52:35 2020
@author: Michael Steiner, EMPA
"""
#%%
"""Import"""
from datetime import datetime
import os, errno
from namelist import *
from read_COSMO import *
from read_COSMO_MESSY import *
from read_WRF import *
from read_FLIGHT import *
from read_WELLS_CLUSTERS import *
from plotting_MAPS import *
from plotting_TIMESERIES import *
from write_NC_HEIGHT import *

#%%
def main():
    ###########################################################################
    # Variables and Parameters:
    ###########################################################################
    model           = "EMPA" #'EMPA', 'DLR' or 'LSCE'
    mission         = "SA" #'SA' or 'INCAS'
    flight_date     = "12.10.2019" #date of the flight
    #The flight-number has to be specified for INCAS-flights on 3rd October (1 or 2)
    #For all other flights, it is ignored and can either be set to a number or to None
    flight_nr       = 1 #None, 1 or 2
    #Select if the map should be on a constant height above s.l. or on a...
    #...constant height above g.l.:
    height_type     = "agl" #"asl" or "agl"
    map_height      = 50 #height of map for plots (m)
    #optional: cut flight to certain temporal boundaries
    flight_start    = None #None or e.g. "14:31:00" (string)
    flight_end      = None #None or e.g. "15:19:00" (string)
    #Switches for plotting:
    plot_maps       = True #maps with flight track: True or False
    plot_timeseries = True #timeseries (incl. curtains): True or False
    plot_staggered  = True #staggered timeseries of CH4-sets: True or False
    #Select which CH4-vars should be plotted on the map (and time series):
    #Use the variable names as used by EMPA.
    #For the DLR and LSCE model, the names are then taken from a look-up table.
    CH4_vars_map    = ["CH4_BG", "CH4_OMV", "CH4_LAKES"]
    #Select sets of CH4-tracers for 'staggered time series plots':
    #(set 1 will be plottet below set 2 etc...)
    CH4_staggered   = [ 
        [
            "CH4_OMV01", "CH4_OMV02", "CH4_OMV03", "CH4_OMV16",
        ], #set 1
        [
            "CH4_OMV06"
        ] #set 2
    ]
    #Choose to add all remaining OMV-tracers as additional set to 'CH4_staggered':
    add_remaining   = True #True or False
    
    
    
    ###########################################################################
    # Process information:
    ###########################################################################
    #Create dates and datestrings:
    flight_date = datetime.strptime(flight_date,"%d.%m.%Y")
    datestring = flight_date.strftime("%Y%m%d")
    if flight_start:
        flight_start = datetime.strptime(
            "%s %s" % (datestring,flight_start),"%Y%m%d %H:%M:%S"
            )
    if flight_end:
        flight_end = datetime.strptime(
            "%s %s" % (datestring,flight_end),"%Y%m%d %H:%M:%S"
            )

    #There are 2 INCAS-flights on 3 Oct. Add flight-nr to name:
    if datestring == "20191003" and mission == "INCAS":
        datestring = datestring + "-%i" % (flight_nr)

    #Create nc-files with height-information: for interpolation
    if plot_maps:
        create_nc_heights(map_height, model, height_type)

    #Create a string with all variable names for the figure:
    var_string = ""
    for itra,tracerstring in enumerate(CH4_vars_map):
        if var_string == "": #add first tracer-name
            var_string += tracerstring
        else: #add '+' character and tracer-name
            var_string += " + " + tracerstring
        if (itra+1) % 6 == 0: #add line-break after 6 variables
            var_string += " \n "

    
    
    ###########################################################################
    # Read flight data and information:
    ###########################################################################
    dicts_flight = readFLIGHT(
        flight_date, flight_start, flight_end, mission, flight_nr
        )
    
    
    
    ###########################################################################
    # Read 2D-field of selected CH-tracers on selected height-level:
    ###########################################################################
    global model_fields
    if plot_maps:
        if model == "EMPA":
            model_fields = readEMPA(
                map_height,
                [x['date_obs'] for x in dicts_flight][0],
                CH4_vars_map
                )
        if model == "DLR":
            model_fields = readDLR(
                map_height,
                [x['date_obs'] for x in dicts_flight][0],
                CH4_vars_map
                )
        if model == "LSCE":
            model_fields = readLSCE(
                map_height,
                [x["date_obs"] for x in dicts_flight][0],
                CH4_vars_map
                )
    
    
    
    ###########################################################################
    # Read polygons of Regions and Cluster and locations of OMV-wells and -facilities:
    ###########################################################################
    dicts_poly = readPOLY()
    dicts_omv  = readWELLS()



    ###########################################################################
    # Read extracted model data and curtains:
    ###########################################################################
    #Add set of additional OMV-tracers if required:
    if add_remaining:
        CH4_staggered.insert(
            0,
            [x for x in all_XY
                if x not in [y for sublist in CH4_staggered for y in sublist]
            ] )

    #Read extracted values and curtains:
    if model == "EMPA":
        dicts_curtain, dicts_extracted = readEXTRACTED_EMPA(
            model,
            flight_date,
            CH4_staggered,
            CH4_vars_map,
            mission,
            flight_start,
            flight_end,
            flight_nr
            )
    if model == "DLR":
        dicts_curtain, dicts_extracted = readEXTRACTED_DLR(
            model,
            flight_date,
            CH4_staggered,
            CH4_vars_map,
            mission,
            flight_start,
            flight_end,
            flight_nr
            )
    if model == "LSCE":
        dicts_curtain = []
        dicts_extracted = []
#        dicts_curtain, dicts_extracted = readEXTRACTED_LSCE(
#            model,
#            flight_date,
#            CH4_staggered,
#            CH4_vars_map,
#            mission,
#            flight_start,
#            flight_end,
#            flight_nr
#            )


#%%    
    ###########################################################################
    # Plot maps with flight track:
    ###########################################################################
    if plot_maps:
        figname = os.path.join(plotdir, "maps", mission, datestring)
        plotMAP(
            model_fields,
            dicts_flight,
            dicts_poly,
            dicts_omv,
            dicts_extracted,
            map_height,
            var_string,
            figname,
            model,
            mission
        )
        
    
    
    ###########################################################################
    # Plot timeseries/curtains:
    ###########################################################################
    if plot_timeseries:
        figname = os.path.join(plotdir, "curtains", mission, datestring)
        plotTIMESERIES(
            dicts_flight,
            dicts_extracted,
            dicts_curtain,
            CH4_vars_map,
            var_string,
            figname,
            model,
            mission
        )
    
    
    
    ###########################################################################
    # Plot staggered plot:
    ###########################################################################
    if plot_staggered:
        CH4_staggered.insert(0,["CH4_BG"])
        figname = os.path.join(plotdir, "staggered", mission, datestring)
        plotSTAGGERED(
            dicts_flight,
            dicts_extracted,
            dicts_curtain,
            CH4_staggered,
            var_string,
            figname,
            model,
            mission,
            add_remaining
        )
    
    
#%%
main()