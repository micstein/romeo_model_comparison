#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 14:12:22 2020
@author: stem
"""
#%%
import numpy as np
from netCDF4 import Dataset
import os
from namelist import *
#%%
def create_nc_heights(map_height,model,height_type):
    
    print("---------------------------------------------------------")
    print("Creating height-files for interpolation...")
    
    targetheight = np.array([map_height])

    #################################################    
    #LOAD THE FILES WITH THE COORDINATES AND HEIGHTS:

    #Define paths and file names:
    if model == "EMPA":
        infile = os.path.join(cosmooutput, "lffd2019092700c.nc")
        hhlfile = infile
    if model=='DLR':
        infile = os.path.join(messyoutput,'ROMEO_ANA_2____20191001_000000_tracer_gp.nc')
        hhlfile = os.path.join(messycurtain,'hhl.nc')
    if model=='LSCE':
        infile = os.path.join(wrfoutput,'WRF_201910150900.nc')
        hhlfile = infile

    #Open files:
    coord  = Dataset(infile)
    height = Dataset(hhlfile)

    #Read data:
    if model=='LSCE':
        HHL = (np.array(height["PH"][0,...])+np.array(height["PHB"][0,...]))/9.81
        lat = np.array(coord["XLAT"][0,...])
        lon = np.array(coord["XLONG"][0,...])
    else:
        HHL = np.array(height["HHL"][:])
        lat = np.array(coord["rlat"][:])
        lon = np.array(coord["rlon"][:])

    #######################################
    #CREATE THE HEIGHT-COORDINATE-VARIABLE:
    
    #EMPA: Make HHL 3D (delete time-dimension) and cut height levels to 25...
    #...for reduced output:
    if model == "EMPA":
        #Delte time-dimension:
        HHL = HHL[0,...]
        #if target height is agl, subtract topography:
        if height_type == "agl":
            for ih in np.arange(HHL.shape[0]):
                HHL[ih,...] -= HHL[-1,...]
        #Select the lowest 26 levels:
        HHL = HHL[::-1,...][0:26,...][::-1,...]
        #Interpolate height from level-boundaries to half-level height:
        HHL = 0.5*(HHL[:-1,...]+HHL[1:,...])

    elif model == "DLR":
        #if target height is agl, subtract topography:
        if height_type == "agl":
            for ih in np.arange(HHL.shape[0]):
                HHL[ih,...] -= HHL[-1,...]
        #Interpolate height from level-boundaries to half-level height:
        HHL = 0.5*(HHL[:-1,...]+HHL[1:,...])

    elif model == "LSCE":
        #Interpolate height from level-boundaries to half-level height:
        HHL = 0.5*(HHL[:-1,...]+HHL[1:,...])
        #if target height is agl, subtract topography:
        if height_type == "agl":
            for ih in np.arange(HHL.shape[0]):
                HHL[ih,...] -= np.array(coord.variables['HGT'][0,...])
    
    ###################################
    #CREATE THE TARGET-HEIGHT-VARIABLE:
    if model=='LSCE':
        htarget = np.zeros((len(targetheight),lat.shape[0],lon.shape[1]))
        for ih in np.arange(htarget.shape[0]):
            htarget[ih,...] = np.full((lat.shape[0],lon.shape[1]),targetheight[ih])
    else:
        htarget = np.zeros((len(targetheight),len(lat),len(lon)))
        for ih in np.arange(htarget.shape[0]):
            htarget[ih,...] = np.full((len(lat),len(lon)),targetheight[ih])
    
    #######################
    #DEFINE THE FILE-NAMES:
    filename = "../data/grids/nc_height.nc"
    filename2 = "../data/grids/hhl.nc"
    
    ###########################################
    #CREATE THE NC-FILE WITH THE TARGET-HEIGHT:
    with Dataset(filename, mode='w') as ofile:
        olon = ofile.createDimension("lon", HHL.shape[2])
        olat = ofile.createDimension("lat", HHL.shape[1])
        olev = ofile.createDimension("level", len(targetheight))
        oh = ofile.createVariable("height", np.float32, ("level","lat","lon"))
        oh.units = "m"
        oh[:] = htarget
    
    ################################################
    #CREATE THE NC-FILE WITH THE HEIGHT-COORDINATES:
    with Dataset(filename2, mode='w') as ofile:
        olon = ofile.createDimension("lon", HHL.shape[2])
        olat = ofile.createDimension("lat", HHL.shape[1])
        olev = ofile.createDimension("level", HHL.shape[0])
        oh = ofile.createVariable("HHL", np.float32, ("level","lat","lon"))
        oh.units = "m"       
        oh[:] = HHL