FILE | DESCRIPTION
-----|------------
**main.py** | Main function <br/> The main switches and settings are located here
*namelist.py* | Namelist for paths, figure properties, domains, tracer names etc... <br/> Also the lookup-table for tracer names is here
**write_OUTPUTGRID.py**| Creates a regular lat/lon output grid (txt-format) for remapping with cdo <br/> Only the lat-/lon-range and number of grids need to be specified
**write_INPUTGRID.py** | Script to create grids of model-output in txt-format for remapping with cdo <br/> This script is used to create the txt-file for the WRF grid <br/> The txt-files for the COSMO and COSMO/Messy grids are more straightforward
write_NC_HEIGHT.py | Script to create a nc-file with the target-height for cdo-interpolation
read_FLIGHT.py | Function to read-in flight-observations (averaged over 20 seconds)
read_COSMO.py | Functions to read-in data from COSMO (output + extracted)
read_COMSO_MESSY.py | Functions to read-in data from COSMO/Messy (output + extracted)
read_WRF.py | Functions to read-in data from WRF (output + extracted)
read_WELLS_CLUSTERS.py |  Functions to read-in the Regions/Clusters (Polygons) and wells/facilities
plotting_MAPS.py | Function to plot the 2D-map of CH4 with the fight-track and -measurements
plotting_TIMESERIES.py | Function to plot the timeseries and curtains of CH4 (and meteo for SA-flights)
auxiliary_functions.py | Functions for grid rotation and calculation of geographical wind

File names in **bold** show executable scripts while the other contain only functions.
