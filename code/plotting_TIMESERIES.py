#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 21:53:31 2020
@author: stem
"""
#%%
import numpy as np
import os, errno
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from scipy.interpolate import interp1d
from namelist import *

#%%
def plotSTAGGERED(
    dicts_flight,
    dicts_extracted,
    dicts_curtain,
    CH4_staggered,
    var_string,
    figname,
    model,
    mission,
    add_remaining
):

    print("---------------------------------------------------------")
    print("Plotting staggered plot...")
    
    ###############################
    #DATE OF FLIGHT FROM EPOCH-TIME
    time_f = [x["time"] for x in dicts_flight][0]
    date_f = []
    for isec in time_f:
        date_f = np.append(date_f, date_ref + timedelta(seconds=int(isec)))

    ##############################
    #DATE OF MODEL FROM EPOCH-TIME
    time_m = [x["time"] for x in dicts_extracted][0]
    date_m = []
    timestamp_m = np.zeros(len(time_m))
    for df in time_m:
        if model == "EMPA":
            date_m = np.append(date_m, date_ref + timedelta(seconds=int(df)))
        if model == "DLR":
            date_m = np.append(
                date_m,
                datetime.strptime(
                    "2019-09-21T00:00",'%Y-%m-%dT%H:%M'
                ) +timedelta(days=float(df))
            )
    for idate,mdate in enumerate(date_m):
        timestamp_m[idate] = (mdate-datetime.utcfromtimestamp(0)).total_seconds()

    #################
    #X-TICK FREQUENCY
    dz = np.nanmax([ int( len(date_f)/11 ) , 1 ])
    
    
    """Plot staggered plot"""
    fig = plt.figure(figsize=(24, 20))
    
    ########################
    ###CH4 OBS + tracer sets
    ax1 = fig.add_subplot(211)
    ###Plot observations:
    obs_f = plt.plot(
        date_f,
        [x["CH4_obs"] for x in dicts_flight][0],
        lw=2.,
        linestyle="-",
        color="k",
        label="OBS",
        zorder=30
    )
    std_f = plt.fill_between(
        date_f,
        [x["CH4_obs"] for x in dicts_flight][0]
            +[x['CH4_std'] for x in dicts_flight][0],
        [x["CH4_obs"] for x in dicts_flight][0]
            -[x["CH4_std"] for x in dicts_flight][0],
        color="k",
        alpha=alpha,
        zorder=30
    )
    
    CH4_cum = np.zeros( #cumulative CH4-values
        (np.array([x["CH4_BG_f"] for x in dicts_extracted][0]).shape)
    )
    
    #Sum-up CH4-tracers:
    for iset,tset in enumerate(CH4_staggered):
        CH4_previous = CH4_cum.copy()
        for it, tracer in enumerate(tset):
            CH4_cum += 0.001*[x[tracer + "_f"] for x in dicts_extracted][0]
        #Plot tracer sets:
        col = colorlist[iset]
        tsline = plt.plot(
            date_m,CH4_cum,
            lw=1.5,linestyle="-",
            color=col,
            label="tracer set %i" % (iset+1)
        )
        tsstd = plt.fill_between(
            date_m,
            CH4_cum,
            CH4_previous,
            color=col,
            alpha=alpha,
            zorder=30
        )
    #x-axis:
    plt.xticks(
        date_f[::dz],
        [x.strftime("%H:%M:%S") for x in date_f][::dz],
        rotation=45
    )
    plt.xticks(fontsize=tsize-3)
    plt.xlim([date_f[0], date_f[-1]])
    ax1.xaxis.set_ticks_position("none")
    plt.setp(ax1.get_xticklabels(), visible=False)
    #y-axis:
    plt.ylim(minCt-0.01, maxCt)
    plt.ylabel("CH4 [ppb]", fontsize=tsize, labelpad=30)
    ax1.tick_params(axis="y", which="major", pad=15)
    plt.yticks(fontsize=tsize-2)
    ax1.yaxis.set_ticks_position("both")
    ###grid:
    plt.grid(b=None, which="major", axis="both")
    ax1.legend(
        fancybox=True,
        framealpha=0.85,
        fontsize=tsize-7,
        loc="center left",
        bbox_to_anchor=(1.,0.5)
    ).set_zorder(102)
    
    #########################
    #CH4 INDIVIDUAL FRACTION
    #(not staggered and on log-scale)
    ax2 = fig.add_subplot(212,sharex=ax1)
    
    #Plot the tracer sets:
    CH4_frac = np.zeros(
        (np.array([x["CH4_BG_f"] for x in dicts_extracted][0]).shape)
    )
    
    if model == "DLR":
        CH4_obs =[x["CH4_obs"] for x in dicts_flight][0]
        fint = interp1d(time_f, CH4_obs, kind='cubic', bounds_error=False)
        CH4_peak = (
            fint(timestamp_m)
                - 0.001*[x["CH4_BG_f"] for x in dicts_extracted][0]
        )
    if model == "EMPA":
        CH4_peak = (
            [
                x["CH4_obs"]
                for x in dicts_flight][0]-0.001*[x["CH4_BG_f"]
                for x in dicts_extracted
            ][0]
        )
    
    for iset,tset in enumerate(CH4_staggered):
        if iset==0: #skip background
            continue
        CH4_cum = np.zeros(
            (np.array([x["CH4_BG_f"] for x in dicts_extracted][0]).shape)
        )
        for it,tracer in enumerate(tset):
            CH4_cum += 0.001*[x[tracer + "_f"] for x in dicts_extracted][0]
        CH4_frac = CH4_cum/CH4_peak
        CH4_frac[CH4_peak<0] = np.nan
        ###Plot tracer sets:
        col = colorlist[iset]
        tsline = plt.plot(
            date_m,
            CH4_frac,
            lw=1.5,
            linestyle="-",
            color=col,
            label="tracer set %i" % (iset+1)
        )
    
    #x-axis:
    plt.xticks(fontsize=tsize-3)
    plt.xlim([date_f[0],date_f[-1]])
    ax2.xaxis.set_tick_params(rotation=45)
    
    #y-axis:
    plt.ylim(1E-4,3E2)
    ax2.tick_params(axis="y", which="major", pad=15)
    plt.ylabel("Fraction of\nOBS - BG", fontsize=tsize,labelpad=30)
    plt.yticks(fontsize=tsize-2)
    ax2.yaxis.set_ticks_position("both")
    ax2.set_yscale("log")
    
    #grid: CH4_staggered
    plt.grid(b=None, which="major", axis="both")
    ax2.legend(
        fancybox=True,
        framealpha=0.85,
        fontsize=tsize-7,
        loc="center left",
        bbox_to_anchor=(1.,0.5)
    ).set_zorder(102)
    
    #Add text about which tracer set contains which tracers:
    for iset,tset in enumerate(CH4_staggered):
        if iset!=1:
            ax2.text(
                1.2,
                -0.29-0.11*iset,
                "Tracer set %i: %s" % (iset+1, CH4_staggered[iset]),
                ha="right",
                size=tsize-9,
                transform=ax2.transAxes
            )
        else: #the last tracer may conatain the remaining OMV-tracers:
            if add_remaining:
                ax2.text(
                    1.2,
                    -0.29
                    -0.11*iset,
                    "Tracer set %i: Remaining XY-Tracers" % (iset+1),
                    ha="right",
                    size=tsize-9,
                    transform=ax2.transAxes
                )
            else:
                ax2.text(
                    1.2,
                    -0.29
                    -0.11*iset,
                   "Tracer set %i: %s" % (iset+1, CH4_staggered[iset]),
                    ha="right",
                    size=tsize-9,
                    transform=ax2.transAxes
                )
    
    ############################
    ###SPACE FOR LABELS AND TEXT
    plt.subplots_adjust(hspace=0.00)
    plt.subplots_adjust(right=0.82)
    plt.subplots_adjust(left=0.15)
    plt.subplots_adjust(bottom=0.24)
    
    ############
    #SAVE FIGURE
    try:
        os.makedirs(figname)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    
    fig.savefig(
        os.path.join(figname, "staggered_%s.png" % (model)),
        dpi=dpival
    )
    fig.clf()
    plt.close()


#%%
def plotTIMESERIES(
    dicts_flight,
    dicts_extracted,
    dicts_curtain,
    CH4_vars_map,
    var_string,
    figname,
    model,
    mission
):
    
    print("---------------------------------------------------------")
    print("Plotting timeseries and curtain...")
   
    ############################
    #SUM-UP SELECTED CH4-TRACERS
    CH4_f = np.zeros(
        (np.array([x[CH4_vars_map[0] + "_f"] for x in dicts_extracted][0]).shape)
    )
    CH4_c = np.zeros(
        (np.array([x[CH4_vars_map[0] + "_c"] for x in dicts_curtain][0]).shape)
    )
    for mapvar in CH4_vars_map:
        CH4_f += 0.001*[x[mapvar + "_f"] for x in dicts_extracted][0]
        cur = [x[mapvar + "_c"] for x in dicts_curtain][0]
        cur[cur == -999.99] = np.nan
        CH4_c += 0.001*cur
    
    ###############################
    #DATE OF FLIGHT FROM EPOCH-TIME
    time_f = [x["time"] for x in dicts_flight][0]
    date_f = []
    for isec in time_f:
        date_f = np.append(date_f, date_ref + timedelta(seconds=int(isec)))
    
    ##############################
    #DATE OF MODEL FROM EPOCH-TIME
    time_m = [x["time"] for x in dicts_extracted][0]
    date_m = []
    for df in time_m:
        if model == "EMPA":
            date_m = np.append(date_m, date_ref + timedelta(seconds=int(df)))
        if model == "DLR":
            date_m = np.append(
                date_m,
                datetime.strptime("2019-09-21T00:00", "%Y-%m-%dT%H:%M")
                    +timedelta(days=float(df))
            )
        
    #################
    #X-TICK FREQUENCY
    dz = np.nanmax([ int( len(date_f)/11 ) , 1 ])

    ############
    #PLOT FIGURE    
    fig    = plt.figure(figsize=(21, 20))
    
    ###############################
    ###FLIGHT-TRACK CH4 (OBS+MODEL)
    ax1 = fig.add_subplot(211)
    obs_f = plt.plot(
        date_f,
        [x["CH4_obs"] for x in dicts_flight][0],
        linestyle="-",
        color="k",
        label="OBS"
    )
    std_f = plt.fill_between(
        date_f,
        [x["CH4_obs"] for x in dicts_flight][0]
            + [x["CH4_std"] for x in dicts_flight][0],
        [x["CH4_obs"] for x in dicts_flight][0]
            - [x["CH4_std"] for x in dicts_flight][0],
        color="k",
        alpha=alpha,
        label="OBS std"
    )
    model_f = plt.plot(
        date_m,
        CH4_f,
        linestyle="-",
        color="red",
        label="%s" % (modellabel[model])
    )
    BG_f = plt.plot(
        date_m,
        0.001*[x["CH4_BG_f"] for x in dicts_extracted][0],
        linestyle='--',
        color="red",
        label="%s BG" % (modellabel[model])
    )
    ax1.text(
        1.,
        1.10,
        "%s" % (var_string),
        va="bottom",
        ha="right",
        rotation="horizontal",
        rotation_mode='anchor',
        fontsize=tsize-7,
        color="k",
        transform=ax1.transAxes
    )
    #x-axis:
    plt.xticks(
        date_f[::dz],
        [x.strftime("%H:%M:%S") for x in date_f][::dz],
        rotation=45
    )
    plt.xticks(fontsize=tsize-3)
    ax1.xaxis.set_ticks_position("none")
    plt.xlim([date_f[0], date_f[-1]])
    plt.setp(ax1.get_xticklabels(), visible=False)
    #y-axis:
    plt.ylabel("CH4 [ppm]", fontsize=tsize, labelpad=30)
    ax1.tick_params(axis="y", which="major", pad=15)
    plt.yticks(fontsize=tsize-2)
    plt.ylim([minCt, maxCt])
    ax1.yaxis.set_ticks_position("both")
    plt.grid(b=None, which="major", axis="both")
    ax1.legend(
        fancybox=True,
        framealpha=0.75,
        fontsize=tsize-10,
        loc="best"
    ).set_zorder(102)
    
    ##########################
    ###2ND SUBPLOT CH4-CURTAIN
    ax2 = fig.add_subplot(212,sharex=ax1)
    #set background to black (makes topography more visible):
    ax2.set_facecolor((0.,0.,0.))
    obs_c = plt.pcolormesh(
        date_m,[x["h"] for x in dicts_flight][0],
        CH4_c.T,
        cmap="jet",
        vmin=minCm,
        vmax=maxCm
    )
    flight = plt.plot(
        date_f,
        0.001*[x["flight_altitude"] for x in dicts_flight][0],
        linestyle="--",
        lw=2,
        color="k"
    )
    #x-axis:
    plt.xticks(
        date_f[::dz],
        [x.strftime("%H:%M:%S") for x in date_f][::dz],
        rotation=45
    )
    plt.xticks(fontsize=tsize-3)
    ax2.xaxis.set_tick_params(rotation=45)
    #y-axis:
    plt.yticks(np.linspace(
        [x['h'] for x in dicts_flight][0][0],
        [x["h"] for x in dicts_flight][0][-1],4),
        np.round(np.linspace([x['h'] for x in dicts_flight][0][0],
        [x["h"] for x in dicts_flight][0][-1],4),1),
        rotation="vertical"
    )
    ax2.tick_params(axis="y", which="major", pad=15)
    plt.ylabel("altitude [km]", fontsize=tsize, labelpad=30)
    plt.yticks(fontsize=tsize-2)
    ax2.yaxis.set_ticks_position("both")
    plt.ylim(
        [
            [x["h"] for x in dicts_flight][0][0],
            [x["h"] for x in dicts_flight][0][-1]
        ]
    )
    #grid:
    plt.grid(b=None, which="major", axis="both")
    
    ###############
    ###ADD COLORBAR
    cbar = fig.colorbar(
        obs_c,
        orientation="horizontal",
        extend="both",
        ticks=np.linspace(minCm, maxCm, 5),
        fraction=frac,
        pad=pad
    )
    cbar.set_label("CH4 [ppm]", rotation=0, fontsize=tsize)
    cbar.ax.tick_params(labelsize=tsize)
    cbar.ax.set_xticklabels(np.round(np.linspace(minCm, maxCm, 5), 2))
    
    ################################
    ###SPACE FOR COLORBAR AND LABELS
    plt.subplots_adjust(hspace=0.00)
    plt.subplots_adjust(right=0.90)
    plt.subplots_adjust(left=0.18)
    
    ############
    #SAVE FIGURE
    try:
        os.makedirs(figname)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    fig.savefig(
        os.path.join(figname, "CH4_%s.png" % (model)),
        dpi=dpival
    )
    fig.clf(); plt.close()
    
    
    
    #################################
    #METEO-CURTAINS AND -FLIGHT-TRACK
    if mission == "SA":
        cvarunits = ["m s-1", "m s-1", "°C"]
        cvarlimst = [[minWt,maxWt], [minWt,maxWt], [minTt,maxTt]]
        cvarlimsc = [[minWm,maxWm], [minWm,maxWm], [minTm,maxTm]]
        cvarcmaps = ["bwr", "bwr", "jet"]
        for icvar, cvar in enumerate(["U", "V", "T"]):
            T_offset = 0.
            if cvar == "T" and model == "DLR":
                T_offset = 273.15
    
            ############
            #PLOT FIGURE    
            fig    = plt.figure(figsize=(21,20))
            
            ###############################
            ###FLIGHT-TRACK CH4 (OBS+MODEL)
            ax1  = fig.add_subplot(211)
            obs_f = plt.plot(
                date_f,
                [x[cvar + "_obs"] for x in dicts_flight][0],
                linestyle='-',
                color="k",
                label="OBS"
            )
            std_f = plt.fill_between(
                date_f,
                [x[cvar + "_obs"] for x in dicts_flight][0]
                    + [x[cvar+"_std"] for x in dicts_flight][0],
                [x[cvar + "_obs"] for x in dicts_flight][0]
                    - [x[cvar + "_std"] for x in dicts_flight][0],
                color="k",alpha=alpha,
                label="OBS std"
            )
            model_f = plt.plot(
                date_m,
                [x[cvar + "_f"] for x in dicts_extracted][0] - T_offset,
                linestyle="-",
                color="red",
                label="%s" % (modellabel[model])
            )
            #x-axis:
            plt.xticks(
                date_f[::dz],
                [x.strftime("%H:%M:%S") for x in date_f][::dz],
                rotation=45
            )
            plt.xticks(fontsize=tsize-3)
            ax1.xaxis.set_ticks_position("none")
            plt.xlim([date_f[0],date_f[-1]])
            plt.setp(ax1.get_xticklabels(),visible=False)
            #y-axis:
            plt.ylabel(
                "%s [%s]" % (cvar,cvarunits[icvar]),
                fontsize=tsize,
                labelpad=30
            )
            ax1.tick_params(axis="y", which="major", pad=15)
            plt.yticks(fontsize=tsize-2)
            plt.ylim(cvarlimst[icvar])
            ax1.yaxis.set_ticks_position("both")
            plt.grid(b=None, which="major", axis="both")
            ax1.legend(
                fancybox=True,
                framealpha=0.75,
                fontsize=tsize-10,
                loc="best"
            ).set_zorder(102)
            
            ##########################
            ###2ND SUBPLOT CH4-CURTAIN
            ax2     = fig.add_subplot(212,sharex=ax1)
            #set background to black (makes topography more visible):
            ax2.set_facecolor((0.,0.,0.))
            cvarcurtain = np.array([x[cvar + "_c"] for x in dicts_curtain][0])
            cvarcurtain[cvarcurtain==-999.99] = np.nan
            obs_c = plt.pcolormesh(
                date_m,
                [x["h"] for x in dicts_flight][0],
                (cvarcurtain-T_offset).T,
                cmap=cvarcmaps[icvar],
                vmin=cvarlimsc[icvar][0],
                vmax=cvarlimsc[icvar][1]
            )
            flight = plt.plot(
                date_f,
                0.001*[x["flight_altitude"] for x in dicts_flight][0], #m-->km
                linestyle="--",
                lw=2,
                color="k"
            )
            #x-axis:
            plt.xticks(
                date_f[::dz],
                [x.strftime("%H:%M:%S") for x in date_f][::dz],
                rotation=45
            )
            plt.xticks(fontsize=tsize-3)
            ax2.xaxis.set_tick_params(rotation=45)
            #y-axis:
            plt.yticks(
                np.linspace(
                    [x["h"] for x in dicts_flight][0][0],
                    [x["h"] for x in dicts_flight][0][-1],4),
                np.round(np.linspace([x["h"] for x in dicts_flight][0][0],
                    [x["h"] for x in dicts_flight][0][-1],4),1),
                rotation="vertical"
            )
            ax2.tick_params(axis="y", which="major", pad=15)
            plt.ylabel("altitude [km]", fontsize=tsize, labelpad=30)
            plt.yticks(fontsize=tsize-2)
            ax2.yaxis.set_ticks_position("both")
            plt.ylim(
                [
                    [x["h"] for x in dicts_flight][0][0],
                    [x["h"] for x in dicts_flight][0][-1]
                ]
            )
            plt.grid(b=None, which="major", axis="both")
            
            ###############
            ###ADD COLORBAR
            cbar = fig.colorbar(
                obs_c,
                orientation="horizontal",
                extend="both",
                ticks=np.linspace(cvarlimsc[icvar][0],cvarlimsc[icvar][1],5),
                fraction=frac,
                pad=pad
            )
            cbar.set_label(
                "%s [%s]" % (cvar,cvarunits[icvar]),
                rotation=0,
                fontsize=tsize
            )
            cbar.ax.tick_params(labelsize=tsize)
            cbar.ax.set_xticklabels(
                np.round(np.linspace(
                    cvarlimsc[icvar][0],cvarlimsc[icvar][1],5),2
                )
            )
            
            ################################
            ###SPACE FOR COLORBAR AND LABELS
            plt.subplots_adjust(hspace=0.00)
            plt.subplots_adjust(right=0.90)
            plt.subplots_adjust(left=0.18)
            
            ############
            #SAVE FIGURE
            fig.savefig(
                os.path.join(figname,"%s_%s.png" % (cvar,model))
                ,dpi=dpival
            )
            fig.clf()
            plt.close()