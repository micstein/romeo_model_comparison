# Analysis scripts for ROMEO simulations and flights

Reading-in flight-measurements of INCAS- and SA-flights

Reading-in 3D model-output and extracted model data along the flight paths of
the models COSMO, COSMO/Messy and WRF

Plotting time series, curtains, maps and contributions of OMV-tracers

## Authors
* Michael Steiner, EMPA, michael.steiner@empa.ch
* Michael Jähn, EMPA, michael.jaehn@empa.ch

## Use
To use the code, just set the required switches in main.py and run the code.
Paths, look-up tables, plotting parameters and further variables can be set in 'namelist.py'.

## Requirements
The following packages are required:

* Python (>= 3.6)
* cartopy
* netCDF4
* numpy
* Cdo integrated into Python
* os
* datetime
* xlrd
* shapely
* bs4
* Scipy
* Matplotlib

## Notice

Please note that only exemplary 3D model output is included in the repository.

The 3D model-output is interpolated to a selected height and remapped onto a regular lat/lon grid.

The files with the grid-definitions are available in 'data/grids'.
Scripts for producingnew grid-definition files are available in 'code/'.

There is a specific README in code/ describing the the scripts.


