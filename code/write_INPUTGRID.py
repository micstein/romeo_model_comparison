#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 09:43:22 2020

@author: stem
"""
#%%
import numpy as np
from netCDF4 import Dataset
import os
from namelist import *
#%%
filename = 'wrfoutputfile.nc'
file = os.path.join(wrfoutput,filename)
data = Dataset(file)

lon = np.array( data.variables['lon'][:] )
lat = np.array( data.variables['lat'][:] )
#%%
nx = lon.shape[1]
ny = lon.shape[0]

file1 = open("../data/grids/wrf_in_grid.txt","w")

file1.write("# Grid of COMSO output \n \n")
file1.write("gridtype = curvilinear \n")
file1.write("gridsize = %i \n"%(nx*ny))
file1.write("xsize = %i \n"%(nx))
file1.write("ysize = %i \n \n"%(ny))
file1.write("# Longitudes \n")
file1.write("xvals = ")
for ilat,latval in enumerate(lon[:,0]):
    file1.write(" ".join(map(str, lon[ilat,:].astype(float))))
    file1.write("\n        ")
file1.write("\n")
file1.write("# Longitudes of cell corners \n")
file1.write("xbounds = \n")
for iy,yy in enumerate(lon[:,0]):
    for ix,xx in enumerate(lon[iy,:]):
        if xx!= lon[iy,-1]:
            dx = .5*(lon[iy,ix+1]-xx)
        xbounds = np.array([xx+dx,xx+dx,xx-dx,xx-dx])
        file1.write(" ".join(map(str, xbounds.astype(float))))
        file1.write("\n")
file1.write("\n")
file1.write("# Latitudes \n")
file1.write("yvals = ")
for ilat,latval in enumerate(lat[:,0]):
    file1.write(" ".join(map(str, lat[ilat,:].astype(float))))
    file1.write("\n        ")
file1.write("\n")
file1.write("# Latitudes of cell corners \n")
file1.write("ybounds = \n")
for iy,yy in enumerate(lat[:,0]):
    for ix,xx in enumerate(lat[iy,:]):
        if xx!= lat[-1,ix]:
            dy = .5*(lat[iy+1,ix]-xx)
        ybounds = np.array([xx-dy,xx+dy,xx+dy,xx-dy])
        file1.write(" ".join(map(str, ybounds.astype(float))))
        file1.write("\n")
        
file1.close()